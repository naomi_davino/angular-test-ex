import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(public authService: AuthService,
    private router: Router) {
    this.authService.getLoginErrors().subscribe(error => {
      this.errorMessage = error;
    });
  }

  email: string;
  password: string;
  public errorMessage: string;
  onSubmit() {
    this.authService.SignUp(this.email, this.password);
    //this.router.navigate(['/books']);
  }

  ngOnInit() {
  }

}

