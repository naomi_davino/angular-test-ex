import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {

  private url = "https://izrs9kdig1.execute-api.us-east-1.amazonaws.com/beta"

  public categories: object = { 0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech' }
  public doc: string;

  userCollection: AngularFirestoreCollection = this.db.collection('users');
  docCollection: AngularFirestoreCollection;



  addarticles(userId: string, doc: string, classify: string) {
    const article = { doc: doc, classify: classify };
    debugger;
    this.userCollection.doc(userId).collection('doc').add(article);
  }

  classify(): Observable<any> {
    let json = {
      "articles": [
        { "text": this.doc }
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url, body).pipe(
      map(res => {
        let final = res.body.replace('[', '')
        final = final.replace(']', '')
        return final;
      })
    )
  }


  getdocs(userId): Observable<any[]> {
    this.docCollection = this.db.collection(`users/${userId}/doc`);
    console.log('Books collection created');
    return this.docCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        data.id = a.payload.doc.id;
        return { ...data };
      }))
    );
  }



  constructor(private http: HttpClient, private db: AngularFirestore) { }
}
