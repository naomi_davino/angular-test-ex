import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-saveclassify',
  templateUrl: './saveclassify.component.html',
  styleUrls: ['./saveclassify.component.css']
})
export class SaveclassifyComponent implements OnInit {

  constructor(public classify: ClassifyService, public auth: AuthService, private router: Router) {

  }

  category: string = "Loading..";
  selected: string;
  userid: string;


  onSubmit() {
    this.classify.addarticles(this.userid, this.classify.doc, this.selected);
    this.router.navigate(['/articles'])
  }

  ngOnInit() {
    this.classify.classify().subscribe(
      res => {
        this.category = this.classify.categories[res];
        this.selected = this.category;
      }
    );

    this.auth.user.subscribe(
      user => {
        this.userid = user.uid;
      }
    )
  }

}
