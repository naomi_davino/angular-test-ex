import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveclassifyComponent } from './saveclassify.component';

describe('SaveclassifyComponent', () => {
  let component: SaveclassifyComponent;
  let fixture: ComponentFixture<SaveclassifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveclassifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveclassifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
