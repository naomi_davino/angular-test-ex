import { Router } from '@angular/router';
import { User } from './interfaces/user';
import { Observable, Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  private logInErrorSubject = new Subject<string>();
  public err;
  constructor(public afAuth: AngularFireAuth,
    private router: Router,private db: AngularFirestore) {
    this.user = this.afAuth.authState;
  }
  userCollection: AngularFirestoreCollection = this.db.collection('users');

  getUser() {
    return this.user
  }
  public getLoginErrors(): Subject<string> {
    return this.logInErrorSubject;
  }
  SignUp(email: string, password: string) {
    this.afAuth
      .auth
      .createUserWithEmailAndPassword(email, password)
      .then(res => {
        console.log('Succesful sign up', res);
        //שמירה של המשתמש בפיירבס
        this.userCollection.doc(res.user.uid).set({ name: res.user.email, id: res.user.uid });
        this.router.navigate(['/welcome']);
      }
      ).catch(
        error => this.err = error
      );

  }

  logout() {
    this.afAuth.auth.signOut();
  }

  login(email: string, password: string) {
    this.afAuth
      .auth.signInWithEmailAndPassword(email, password)
      .then(
        res => {
          console.log('Succesful Login', res);
          this.router.navigate(['/welcome']);
        }
      ).catch(
        error => this.err = error
      )
  }


}
