import { AuthService } from './../auth.service';
import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-collection-class',
  templateUrl: './collection-class.component.html',
  styleUrls: ['./collection-class.component.css']
})
export class CollectionClassComponent implements OnInit {

  
  docs$:Observable<any>;
  userId:string;

  constructor(private classify:ClassifyService,public auth:AuthService) { }

  ngOnInit() {
        this.auth.user.subscribe(
          user => {
            this.userId = user.uid;
        this.docs$ = this.classify.getdocs(this.userId);
           }
        )
  }

}
